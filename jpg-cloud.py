#!/usr/bin/env python3
############################### BY GIBBONJOYEUX ################################ 

WIDTH           = 1920
HEIGHT          = 1080
RATIO           = WIDTH / HEIGHT
RATIO_REDUCTION = 5
MAX_SIZE        = 100 * 1024

################################################################################
### MODULES
################################################################################

import  subprocess as sub
import  os

OUTPUT_DIR  = "output/"
IMAGES_DIR  = "images/"

################################################################################
### FUNCTIONS
################################################################################

def         get_image_resolution(image):
    width = sub.check_output(["magick", "convert", image,
    "-ping", "-format", "%[fx:w]", "info:"])
    height = sub.check_output(["magick", "convert", image,
    "-ping", "-format", "%[fx:h]", "info:"])
    return (int(width), int(height))

def         resize_image(path, name, width, height):
    resolution = "{}x{}".format(width, height)
    output = "{}/{}-{}.jpg".format(OUTPUT_DIR, name, resolution)
    sub.run(["magick", "convert", path,
    "-resize", "{}".format(resolution),
    output])
    return output

def         create_base_image(image_path, output_path):
    (width, height) = get_image_resolution(image_path)
    original_ratio = width / height
    ### CREATE BASE IMAGE
    ### IF DIFFERENT RATIO
    if original_ratio != RATIO:
        if original_ratio > RATIO:
            width = int(height * RATIO)
        else:
            height = int(width / RATIO)
        sub.run(["magick", "convert", image_path,
        "-crop", "{}x{}+0+0".format(width, height),
        "-resize", "{}x{}".format(WIDTH, HEIGHT),
        output_path])
    ### IF SAME RATIO
    else:
        sub.run(["magick", "convert", image_path,
        "-resize", "{}x{}".format(WIDTH, HEIGHT),
        output_path])

def         handle_image(image_path):
    name = os.path.splitext(os.path.basename(image_path))[0]
    path = "{}/{}.jpg".format(OUTPUT_DIR, name)
    create_base_image(image_path, path)
    ratio = 100
    while True:
        # TODO: store ratio + size into
        ratio -= RATIO_REDUCTION
        new_image = resize_image(path, name, int(WIDTH * ratio / 100),
        int(HEIGHT * ratio / 100))
        size = os.stat(new_image).st_size
        if size <= MAX_SIZE:
            break

################################################################################
### MAIN
################################################################################

images = os.listdir(IMAGES_DIR)
for image in images:
    handle_image(IMAGES_DIR + image)
