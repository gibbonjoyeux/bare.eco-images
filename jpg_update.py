#!/usr/bin/env python3
############################### BY GIBBONJOYEUX ################################ 

OUTPUT_DIR  = "output/"
SVG_PATH    = "output/graph.svg"

################################################################################
### MODULES
################################################################################

import  os
import  sys
import  re

sys.path.append(".")

from jpg import svg

################################################################################
### FUNCTIONS
################################################################################

################################################################################
### MAIN
################################################################################

output = os.listdir(OUTPUT_DIR)
output.sort()
data = []
current_image = None
prev_index = None
### GET DATA
for item in output:
    regex = re.search(r"(\d+)-(\d+)x(\d+)-(\d+).jpg", item)
    if regex == None:
        continue
    index, width, height, quality = regex.groups()
    if prev_index != index:
        prev_index = index
        if current_image != None:
            data.append(current_image)
        current_image = []
    current_image.append(((int(width), int(height)), int(quality)))
data.append(current_image)
### CREATE SVG
svg.create(data, SVG_PATH)
