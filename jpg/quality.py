#!/usr/bin/env python3
############################### BY GIBBONJOYEUX ################################ 

################################################################################
### MODULES
################################################################################

import      os
import      sys
import      subprocess as sub

################################################################################
### FUNCTIONS
################################################################################

def         adapt_quality(path, max_size):
    quality = 50
    step = 25
    tested = {}

    ### BEGIN SEARCH
    while step > 0:
        ### CREATE FILE
        tmp_output = "tmp/{}.jpg".format(quality)
        if not tmp_output in tested:
            sub.run(["magick", "convert", path, "-quality", "{}%".format(quality), tmp_output])
            tested[tmp_output] = quality
        ### GET SIZE
        current_size = os.stat(tmp_output).st_size
        if current_size == max_size:
            break
        elif current_size > max_size:
            quality = max(1, quality - step)
        else:
            quality = min(100, quality + step)
        ### SHARPEN STEP
        if step != 1 and step % 2 == 1:
            step = step // 2 + 1
        else:
            step //= 2

    ### LAST PRECISION
    if current_size < max_size:
        tmp_output = "tmp/{}.jpg".format(quality)
        if not tmp_output in tested:
            sub.run(["magick", "convert", path, "-quality", "{}%".format(quality), tmp_output])
            tested[tmp_output] = quality
        current_size = os.stat(tmp_output).st_size

    ### FIND MORE ADAPTED
    tested = list(tested.items())
    tested.sort(key = lambda item : item[1])
    best_name = None
    to_remove = []
    for item in tested:
        if os.stat(item[0]).st_size < max_size:
            if best_name != None:
                to_remove.append(best_name)
            best_name = item[0]
            best_quality = item[1]
        else:
            to_remove.append(item[0])

    ### OUTPUT
    if best_name != None:
        name = os.path.splitext(os.path.basename(path))[0]
        output = "output/{}-{:03}.jpg".format(name, best_quality)
        sub.run(["mv", best_name, output])

    ### REMOVE TMP FILE
    sub.run(["rm", "-f"] + to_remove)

    return best_name != None and best_quality or 0
