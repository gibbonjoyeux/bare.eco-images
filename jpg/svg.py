#!/usr/bin/env python3
############################### BY GIBBONJOYEUX ################################ 

SVG_HEADER          = """<svg viewBox="0 0 {0} {1}" xmlns="http://www.w3.org/2000/svg">
\t<style>
\t\tpolyline {{
\t\t\tstroke-width:	2;
\t\t\tstroke-linecap:   round;
\t\t}}

\t\tcircle {{
\t\t\tvisibility:	hidden;
\t\t}}

\t\t.line:hover polyline {{
\t\t\tstroke-width:	5;
\t\t}}

\t\t.line:hover circle {{
\t\t\tvisibility:	visible;
\t\t\tfill:             black;
\t\t}}
\t</style>
\t<polygon points="0,0 {0},0 {0},{1} 0,{1}" fill="none" stroke="black" />
\t<g fill="none">
"""
SVG_FOOTER          = "\t</g>\n</svg>"
SVG_COLOR_HEADER    = "\t\t<g stroke=\"{}\">\n"
SVG_COLOR_FOOTER    = "\t\t</g>"
SVG_LINE_OBJECT     = "\t\t\t<g class=\"line\">\n\t\t\t\t<polyline points=\"{}\" />\n{}\t\t\t</g>\n"
SVG_LINE            = "{},{} "
SVG_POINT           = "\t\t\t\t<circle cx=\"{}\" cy=\"{}\" r=\"4\"><title>{}x{} {}%</title></circle>\n"
SVG_WIDTH           = 500
SVG_HEIGHT          = 500
SVG_MARGIN_X        = 10
SVG_MARGIN_Y        = 10
COLORS              = ["#721b65", "#b80d57", "#f8615a", "#ffd868"]

################################################################################
### MODULES
################################################################################

################################################################################
### FUNCTIONS
################################################################################

def         get_position(src_min, src_max, dst_max, margin, value):
    return int(((value - src_min) / (src_max - src_min)) * (dst_max - margin * 2) + margin)

def         get_boundaries(data):
    min_x = None
    max_x = None
    min_y = None
    max_y = None
    for image in data:
        for combination in image:
            (resolution, param) = combination
            ### GET Y
            if min_y == None:
                min_y = resolution
                max_y = resolution
            if min_y[0] > resolution[0]:
                min_y = resolution
            if max_y[0] < resolution[0]:
                max_y = resolution
            ### GET X
            if min_x == None:
                min_x = param
                max_x = param
            if min_x > param:
                min_x = param
            if max_x < param:
                max_x = param
    return (min_x, max_x, min_y, max_y)

def         draw_graph(output, data):
    (min_x, max_x, min_y, max_y) = get_boundaries(data) 
    colors = [""] * len(COLORS)
    color = 0
    i = 0
    ### STORE LINES
    for image in data:
        #line = SVG_LINE_HEADER.format(COLORS[color])
        line = ""
        points = ""
        for combination in image:
            (resolution, param) = combination
            pos_x = get_position(min_x, max_x, SVG_WIDTH, SVG_MARGIN_X, param)
            pos_y = get_position(min_y[0], max_y[0], SVG_HEIGHT, SVG_MARGIN_Y, max_y[0] - resolution[0] + min_y[0])
            points += SVG_POINT.format(pos_x, pos_y, resolution[0], resolution[1], param)
            line += SVG_LINE.format(pos_x, pos_y)
        string = SVG_LINE_OBJECT.format(line, points)
        colors[color] += string
        color = (color + 1) % len(COLORS)
        i += 1
    ### PUT LINES WITH COLORS
    i = 0
    for color in colors:
        output.write(SVG_COLOR_HEADER.format(COLORS[i]))
        output.write(color)
        output.write(SVG_COLOR_FOOTER)
        i += 1

def         create(data, output_path):
   output = open(output_path, "w")
   output.write(SVG_HEADER.format(SVG_WIDTH, SVG_HEIGHT))
   draw_graph(output, data)
   output.write(SVG_FOOTER)
   output.close()
