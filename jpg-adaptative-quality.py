#!/usr/bin/env python3
############################### BY GIBBONJOYEUX ################################ 

################################################################################
### MODULES
################################################################################

import      os
import      sys
import      subprocess as sub

################################################################################
### FUNCTIONS
################################################################################

def         error(message, quit):
    print("{}: {}".format(__file__, message))
    if quit == True:
        sys.exit(1)

def         print_size(size):
    if size > 1024:
        if size > 1024 * 1024:
            print("{:.1f} mb".format(size / (1024 * 1024)))
        else:
            print("{:.1f} kb".format(size / 1024))
    else:
        print("{} b".format(size))

def         adapt_quality(file_name, max_size):
    quality = 50
    step = 25
    tested = {}

    ### BEGIN SEARCH
    while step > 0:
        ### CREATE FILE
        tmp_output = "tmp/{:03}.jpg".format(quality)
        if not tmp_output in tested:
            sub.run(["magick", "convert", file_name, "-quality", "{}%".format(quality), tmp_output])
            tested[tmp_output] = quality
        ### GET SIZE
        current_size = os.stat(tmp_output).st_size
        if current_size == max_size:
            break
        elif current_size > max_size:
            quality = max(1, quality - step)
        else:
            quality = min(100, quality + step)
        ### SHARPEN STEP
        if step != 1 and step % 2 == 1:
            step = step // 2 + 1
        else:
            step //= 2
        sys.stdout.flush()

    ### LAST PRECISION
    if current_size < max_size:
        tmp_output = "tmp/{:03}.jpg".format(quality)
        if not tmp_output in tested:
            sub.run(["magick", "convert", file_name, "-quality", "{}%".format(quality), tmp_output])
            tested[tmp_output] = quality
        current_size = os.stat(tmp_output).st_size

    ### FIND MORE ADAPTED
    tested = list(tested.items())
    tested.sort(key = lambda item : item[1])
    best_name = None
    to_remove = []
    for item in tested:
        if os.stat(item[0]).st_size < max_size:
            if best_name != None:
                to_remove.append(best_name)
            best_name = item[0]
            best_quality = item[1]
        else:
            to_remove.append(item[0])

    ### OUTPUT
    if best_name != None:
        output = "output/{}-{}".format(os.path.splitext(os.path.basename(file_name))[0], os.path.basename(best_name))
        sub.run(["mv", best_name, output])

    ### REMOVE TMP FILE
    sub.run(["rm", "-f"] + to_remove)

    return best_name != None and best_quality or 0


################################################################################
### MAIN
################################################################################

### GET PARAMETERS
if len(sys.argv) != 4:
    error("Wrong number of arguments", True)
file_name = sys.argv[1]
size = int(sys.argv[2])
if sys.argv[3] == "kb":
    size *= 1024
elif sys.argv[3] == "mb":
    size *= 1024 * 1024
elif not sys.argv[3] == "b":
    error("Wrong unit for max size", True)

### CREATE SANDBOX DIRECTORY
if not os.path.isdir("tmp/"):
    os.mkdir("tmp/")
### CREATE OUTPUT DIRECTORY
if not os.path.isdir("output/"):
    os.mkdir("output/")

print(adapt_quality(file_name, size))
