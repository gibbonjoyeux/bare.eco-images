#!/usr/bin/env python3
############################### BY GIBBONJOYEUX ################################ 

################################################################################
### MODULES
################################################################################

import          subprocess as sub
import          os
import          sys

sys.path.append(".")

from jpg import quality
from jpg import svg

################################################################################
### DATA
################################################################################

OUTPUT_DIR      = "output/"
IMAGES_DIR      = "images/"
SVG_PATH        = "output/graph.svg"
WIDTH           = 1920
HEIGHT          = 1080
RATIO           = WIDTH / HEIGHT
RATIO_REDUCTION = 5
MAX_SIZE        = 100 * 1024

################################################################################
### FUNCTIONS
################################################################################

def         print_image_data(data):
    for elem in data:
        print("{:4} x {:4} : {}%".format(elem[0][0], elem[0][1], elem[1]))
    sys.stdout.flush()

def         get_image_resolution(image):
    width = sub.check_output(["magick", "convert", image,
    "-ping", "-format", "%[fx:w]", "info:"])
    height = sub.check_output(["magick", "convert", image,
    "-ping", "-format", "%[fx:h]", "info:"])
    return (int(width), int(height))

def         resize_image(path, index, width, height):
    resolution = "{:04}x{:04}".format(width, height)
    output = "{}/{:03}-{}.jpg".format(OUTPUT_DIR, index, resolution)
    sub.run(["magick", "convert", path,
    "-resize", "{}".format(resolution),
    output])
    return output

def         create_base_image(image_path, output_path):
    (width, height) = get_image_resolution(image_path)
    original_ratio = width / height
    ### CREATE BASE IMAGE
    ### IF DIFFERENT RATIO
    if original_ratio != RATIO:
        if original_ratio > RATIO:
            width = int(height * RATIO)
        else:
            height = int(width / RATIO)
        sub.run(["magick", "convert", image_path,
        "-crop", "{}x{}+0+0".format(width, height),
        "-resize", "{}x{}".format(WIDTH, HEIGHT),
        output_path])
    ### IF SAME RATIO
    else:
        sub.run(["magick", "convert", image_path,
        "-resize", "{}x{}".format(WIDTH, HEIGHT),
        output_path])

def         handle_image(image_path, index):
    path = "{}/{:03}-{:04}x{:03}.jpg".format(OUTPUT_DIR, index, WIDTH, HEIGHT)
    ### CREATE BASE IMAGE
    create_base_image(image_path, path)
    ### REDUCE IMAGE UNTIL REACH LIMIT
    to_remove = []
    data = []
    ratio = 100
    while True:
        width = int(WIDTH * ratio / 100)
        height = int(HEIGHT * ratio / 100)
        if ratio < 100:
            new_image = resize_image(path, index, width, height)
        else:
            new_image = path
        size = os.stat(new_image).st_size
        if size <= MAX_SIZE:
            sub.run(["mv", new_image, "{}/{:03}-{:04}x{:04}-100.jpg".format(OUTPUT_DIR, index, width, height)])
            data.append(((width, height), 100))
            break
        else:
            to_remove.append(new_image)
            result = quality.adapt_quality(new_image, MAX_SIZE)
            if result > 0:
                data.append(((width, height), result))
        ratio -= RATIO_REDUCTION
    if len(to_remove) > 0:
        sub.run(["rm"] + to_remove)
    return (data)

################################################################################
### MAIN
################################################################################

images_data = []
images = os.listdir(IMAGES_DIR)
i = 0
for image in images:
    image_data = handle_image(IMAGES_DIR + image, i)
    images_data.append(image_data)
    print(image)
    print_image_data(image_data)
    i += 1
svg.create(images_data, SVG_PATH)
